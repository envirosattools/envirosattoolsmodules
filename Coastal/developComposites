/**** Start of imports. If edited, may not auto-convert in the playground. ****/
var geometry = /* color: #ff0000 */ee.Geometry.LineString(
        [[174.47374797504142, -37.001840843791435],
         [174.47254634540275, -37.01198494179971],
         [174.4749496046801, -37.02418348265727],
         [174.47426295917228, -37.03254314916197],
         [174.47992778461173, -37.04254613080987],
         [174.4833610121508, -37.04953373330393],
         [174.4895408217211, -37.05309579289921],
         [174.49778056781486, -37.05323279185251],
         [174.50413203876212, -37.05062976944783]]);
/***** End of imports. If edited, may not auto-convert in the playground. *****/
// Set map center to Whatipu
Map.setCenter(174.48185226957992, -37.038503988671415, 13);

// global variables including OTSU module
var band_names = ['blue', 'green', 'red', 'RE1', 'RE2', 'RE3', 'NIR', 'RE4', 'SWIR1', 'SWIR2'];
var composite_1;
var composite_2;
var roi;

/// IMAGE FUNCTIONS ///
// function to rename required image bands
function RenameBandsS2(image) {
  return image.slice(1,11).rename(band_names);
}

// function to resample 10 m bands to 20 m and define NZTM projection
var crs = 'EPSG:2193';
function resample_to_low_res(image) {
  var bands = image.bandNames();
  var resampled_bands = image.select(bands).reproject({crs: crs, scale: 20.0});
  return resampled_bands;
}

// Function to mask clouds using QA60 band
var maskClouds = function(image){
var qa = image.select('QA60');
  // Bits 10 and 11 are clouds and cirrus, respectively.
  var cloudBitMask = 1 << 10;
  var cirrusBitMask = 1 << 11;
  // clear if both flags set to zero.
  var mask = qa.bitwiseAnd(cloudBitMask).eq(0)
    .and(qa.bitwiseAnd(cirrusBitMask).eq(0));
return image.updateMask(mask);
};

// Functions to apply indices
var applyIndices = {
  // apply ndvi to image
  applyNDVI: function(image) {
    var calcNDVI = image.normalizedDifference(['NIR','red']);
    var ndvi = calcNDVI.select([0], ['ndvi']);
    return ee.Image.cat([image, ndvi]);
  },
  // apply mndwi to image
  applyMNDWI: function(image) {
    var calcMNDWI = image.normalizedDifference(['green','SWIR1']);
    var mndwi = calcMNDWI.select([0], ['mndwi']);
    return ee.Image.cat([image, mndwi]);
  }
};
/// END OF IMAGE FUNCTIONS ///

// Make polyline extraction interactive
// get drawing tools widget
var drawingTools = Map.drawingTools();
drawingTools.setShown(false);

// clear all existing imports from drawing tools
while (drawingTools.layers().length() > 0) {
  var layer = drawingTools.layers().get(0);
  drawingTools.layers().remove(layer);
}

// Initialize dummy geometry to act as placeholder for drawn geometries
var dummyGeometry = 
  ui.Map.GeometryLayer({geometries: null, name: 'geometry', color: 'ff0000'});
  
drawingTools.layers().add(dummyGeometry);

// define geometry clearing function
function clearGeometry() {
  var layers = drawingTools.layers();
  layers.get(0).geometries().remove(layers.get(0).geometries().get(0));
}

// define function to initialize drawing of polyline features 
function drawLine() {
  clearGeometry();
  drawingTools.setShape('line');
  drawingTools.draw();
}

// Define function that generates composite once geometry is defined
function GenerateComposite() {
  // define composite IDs
  var ID_1 = idBox1.getValue();
  var ID_2 = idBox2.getValue();
  // clear map view of previous images
  Map.clear();
  // get drawn geometry representing approximate shoreline and buffer
  var approx_shoreline = drawingTools.layers().get(0).getEeObject();
  // define roi with buffer
  var roi = approx_shoreline.buffer(1500);
  // set drawing tools back to null to turn off drawing
  drawingTools.setShape(null);
  
  // Create Sentinel-2 SR image collection for time instance covering roi including bands required for classification 
  var S2_scenes = ee.ImageCollection("COPERNICUS/S2_SR")
    .filterBounds(roi)
    .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 30))
    .filterMetadata('SENSING_ORBIT_DIRECTION', 'equals', 'DESCENDING')
    .map(function(image){return image.clip(roi)})
    .map(maskClouds)
    .map(RenameBandsS2)
    //.map(resample_to_low_res)
    .map(applyIndices.applyNDVI)
    .map(applyIndices.applyMNDWI);
  
  // filter scenes by date to gather collection
  var collection1 = S2_scenes.filterDate(ee.Date(startBox1.getValue()), ee.Date(endBox1.getValue()));
  var collection2 = S2_scenes.filterDate(ee.Date(startBox2.getValue()), ee.Date(endBox2.getValue()));
  
  // function to get the number of scenes
  function countImages(collection, ID) {
  var count = collection.size();
  // print number of scenes in composite to controlPanel
  var ImageCountLabel = ui.Label(ID + ' image count: ' + collection.size().getInfo());
  //imageCount.clear();
  imageCount.add(ImageCountLabel);
  }
  countImages(collection1, ID_1);
  countImages(collection2, ID_2);
  // function to perform median reductions and calculate indices and output composite
  function reduce(collection) {
  //  generate percentile composite using 15th percentile across S2 bands
  var image_bands = collection.select(['blue', 'green', 'red', 'RE1', 'RE2', 'RE3', 'NIR', 'RE4', 'SWIR1', 'SWIR2']);
  var median = image_bands.reduce(ee.Reducer.percentile([15])).rename(band_names);
  //  calculate NDVI and MNDWI
  var ndvi = collection.select(['ndvi']).reduce(ee.Reducer.intervalMean(10,90)).rename('ndvi');
  var mndwi = collection.select(['mndwi']).reduce(ee.Reducer.percentile([25])).rename('mndwi');
  var composite = median
    .addBands(ndvi)
    .addBands(mndwi);
  return composite;
  }
  composite_1 = reduce(collection1);
  composite_2 = reduce(collection2);
  Map.addLayer(composite_1, {bands: ['red', 'green', 'blue'], min:0, max:1000}, ID_1);
  Map.addLayer(composite_2, {bands: ['red', 'green', 'blue'], min:0, max:1000}, ID_2);
}

// Define function to export composite
function exportComposite() {
  Export.image.toAsset({
    image: composite_1,
    description: 'export_' + idBox1.getValue(),
    assetId: idBox1.getValue(),
    scale: 20,
    crs: crs,
    maxPixels: 1e13,
    region: roi,
  });
  Export.image.toAsset({
    image: composite_2,
    description: 'export_' + idBox2.getValue(),
    assetId: idBox2.getValue(),
    scale: 20,
    crs: crs,
    maxPixels: 1e13,
    region: roi,
  });
}

// set drawing tools widget to listen for geometry drawing & editing events
// & respond with GenerateComposite
//drawingTools.onDraw(ui.util.debounce(GenerateComposite, 500));
//drawingTools.onEdit(ui.util.debounce(GenerateComposite, 500));

/// DEVELOP UI ///
// Create startBox & endBox variables
var idBox1 = ui.Textbox({
      placeholder: 'composite ID',
      style: {stretch: 'horizontal'}
    });
var startBox1 = ui.Textbox({
      placeholder: 'YYYY-MM-DD',
      style: {stretch: 'horizontal'}
    });
var endBox1 = ui.Textbox({
      placeholder: 'YYYY-MM-DD',
      style: {stretch: 'horizontal'}
    });
var idBox2 = ui.Textbox({
      placeholder: 'composite ID',
      style: {stretch: 'horizontal'}
    });
var startBox2 = ui.Textbox({
      placeholder: 'YYYY-MM-DD',
      style: {stretch: 'horizontal'}
    });
var endBox2 = ui.Textbox({
      placeholder: 'YYYY-MM-DD',
      style: {stretch: 'horizontal'}
    });
 
// define panel to contain Composite metadata   
var imageCount = ui.Panel([]);

// Button labels are concatenations of symbols in previous step & text
// Set onClick parameter to drawing mode callback fuction defined above
// Generate UI panel to hold instructions, drawing buttons and textboxes for date ranges
var controlPanel = ui.Panel({
  widgets: [
    ui.Label('1. Draw approximate shoreline'),
    ui.Button({
      label: 'Draw Polyline',
      onClick: drawLine,
      style: {stretch: 'horizontal'}
    }),
    ui.Label('2. Create composites for change detection'),
    ui.Label('start composite'),
    idBox1,
    startBox1,
    endBox1,
    ui.Label('end composite'),
    idBox2,
    startBox2,
    endBox2,
    ui.Button({
      label: 'Generate Composites',
      onClick: GenerateComposite,
      style: {stretch: 'horizontal'}
    }),
    ui.Label('3. Export Composites'),
    ui.Button({
      label: 'Export Composites',
      onClick: exportComposite,
      style: {stretch: 'horizontal'}
    }),
    ui.Label({
      value: 'Composite metadata',
      style: {fontSize: '24px',
              fontWeight: 'bold'}
      }),
    imageCount,
  ],
  style: {position: 'bottom-left'},
  layout: null
});

ui.root.insert(0, controlPanel);

