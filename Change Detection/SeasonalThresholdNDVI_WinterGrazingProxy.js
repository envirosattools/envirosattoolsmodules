/*

!!The task takes a long time to run, because a cloud shadow masking function 
  projects shadows from clouds, at a fixed spatial resolution on server side.

Simple, non-supervised identification of likely location of land with high autumn NDVI
and low NDVI by following spring.

Creates two mosaics:
- average pixel value for clear pixels between two dates (autumn periods)
- most recent clear pixel between two dates (winter period)

Calculates the difference in NDVI between the two images

The cloud and shadow masking are copied from the following tutorial:
https://developers.google.com/earth-engine/tutorials/community/sentinel-2-s2cloudless

Developed to eyeball performance of a model that uses Feb-May Sentinel-2 data to predict the 
likely locaiton of winter forage grazing crops (fodder beet, kale, and other brassicas). 

*/

var palettes = require('users/gena/packages:palettes');
var palette = palettes.matplotlib.inferno[7];

var aoi = ee.Geometry.Point([172.228,-43.425]); //([171.1642, -43.4734]); /

// exmaple years
var start_date_winter = '2020-07-01';
var end_date_winter = '2020-09-14';
var start_data_autumn = "2020-02-15";
var end_date_autumn = "2020-04-30";

// cloud filter and cloud shadow parameters
// tune these parameters
// if you are seeing masked areas or incursion of cloud shadow

var cloud_filter = 30;
var cld_prb_thresh = 40;
var nir_drk_thresh = 0.30;
var cld_prj_dist = 4;
var buffer = 300;

var get_s2_sr_cld_col = function(aoi, start_date, end_date){
    // Import and filter S2 SR.
    var s2_sr_col = (ee.ImageCollection('COPERNICUS/S2_SR')
        .filterBounds(aoi)
        .filterDate(start_date, end_date)
        .filter(ee.Filter.lte('CLOUDY_PIXEL_PERCENTAGE', cloud_filter)));

    // Import and filter s2cloudless.
    var s2_cloudless_col = (ee.ImageCollection('COPERNICUS/S2_CLOUD_PROBABILITY')
        .filterBounds(aoi)
        .filterDate(start_date, end_date));
    var join_condition = ee.Filter.equals({leftField:'system:index',
                                          rightField:'system:index'});
    // Join the filtered s2cloudless collection to the SR collection by the 'system:index' property.
    var join_set = ee.Join.saveFirst('s2cloudless')
                          .apply({
                            primary:s2_sr_col,
                            secondary:s2_cloudless_col,
                            condition:join_condition});
    return ee.ImageCollection(join_set);                      
}


var add_cloud_bands= function(img){
  var cld_prb = ee.Image(img.get('s2cloudless')).select("probability");
  var is_cloud = cld_prb.gt(cld_prb_thresh).rename("clouds");
  return img.addBands(ee.Image([cld_prb, is_cloud]));
};


var add_shadow_bands = function(img){
    // Identify water pixels from the SCL band.
    var not_water = img.select('SCL').neq(6)

    // Identify dark NIR pixels that are not water (potential cloud shadow pixels).
    var sr_band_scale = 1e4
    var dark_pixels = img.select('B8').lt(nir_drk_thresh*sr_band_scale).multiply(not_water).rename('dark_pixels')

    // Determine the direction to project cloud shadow from clouds (assumes UTM projection).
    var shadow_azimuth = ee.Number(90).subtract(ee.Number(img.get('MEAN_SOLAR_AZIMUTH_ANGLE')));

    // Project shadows from clouds for the distance specified by the CLD_PRJ_DIST input.
    var cld_proj = (img.select('clouds')
                    .directionalDistanceTransform(shadow_azimuth, cld_prj_dist*10)
        .reproject({'crs': img.select(0).projection(), 'scale': 100})
        .select('distance')
        .mask()
        .rename('cloud_transform'));

    // Identify the intersection of dark pixels with cloud shadow projection.
    var shadows = cld_proj.multiply(dark_pixels).rename('shadows');

    // Add dark pixels, cloud projection, and identified shadows as image bands.
    return img.addBands(ee.Image([dark_pixels, cld_proj, shadows]));

}


var add_cld_shadow_mask = function(img){
    // Add cloud component bands.
    var img_cloud = add_cloud_bands(img);

    //# Add cloud shadow component bands.
    var img_cloud_shadow = add_shadow_bands(img_cloud);

    // Combine cloud and shadow mask, set cloud and shadow as value 1, else 0.
    var is_cld_shdw = img_cloud_shadow
                        .select('clouds')
                        .add(img_cloud_shadow.select('shadows'))
                        .gt(0);

    // Remove small cloud-shadow patches and dilate remaining pixels by BUFFER input.
    // 20 m scale is for speed, and assumes clouds don't require 10 m precision.
    is_cld_shdw = (is_cld_shdw.focal_min(2).focal_max(buffer*2/20)
                       .reproject({'crs': img.select([0]).projection(), 'scale': 20})
                      .rename('cloudmask'));

    // Add the final cloud-shadow mask to the image.
    return img_cloud_shadow.addBands(is_cld_shdw).addBands(img.metadata('system:time_start'));
};

var apply_cld_shadow_mask = function(img){
    // Subset the cloudmask band and invert it so clouds/shadow are 0, else 1.
    var not_cld_shdw = img.select('cloudmask').not();

    // Subset reflectance bands and update their masks, return the result.
    return img.select('B.*').addBands(img.select('system:time_start')).updateMask(not_cld_shdw);
};

//
//
// Make a winter mosaic ...
// 
var s2_sr_cld_col_winter = get_s2_sr_cld_col(aoi, start_date_winter, end_date_winter);
var s2_sr_winter = s2_sr_cld_col_winter.map(add_cld_shadow_mask)
                                     .map(apply_cld_shadow_mask)
                                    .qualityMosaic("system:time_start");


s2_sr_winter = s2_sr_winter.addBands(s2_sr_winter.normalizedDifference(['B5', 'B4']))

//
//
// Make an autumn mosaic ...
// 
var s2_sr_cld_col_autumn = get_s2_sr_cld_col(aoi, start_data_autumn, end_date_autumn);
var s2_sr_autumn = s2_sr_cld_col_autumn.map(add_cld_shadow_mask)
                                     .map(apply_cld_shadow_mask)
                                     .map(function (img) {
                                       return img.addBands(img.normalizedDifference(['B5', 'B4']));
                                     })
                                     .median();
// Plot mosaics
Map.addLayer(s2_sr_winter,
              {'bands': ['B4', 'B3', 'B2'], 'min': 0, 'max': 2500, 'gamma': 1.1},
              'S2 cloud-free: most recent mosaic');

Map.addLayer(s2_sr_autumn,
              {'bands': ['B4', 'B3', 'B2'], 'min': 0, 'max': 2500, 'gamma': 1.1},
              'S2 cloud-free: autumn mosaic');

// Calculate change in NDVI
var autumn_nd = s2_sr_autumn.select("nd").rename("autumn_nd");
var winter_nd = s2_sr_winter.select("nd").rename("winter_nd");
var change = autumn_nd.subtract(winter_nd).rename("change");
// normalised prior and post ND, for visualisation
var change_nd = ee.Image([autumn_nd, winter_nd]).normalizedDifference().rename("change_nd")

// can make a threshold product to using the above 'change' image.

// or consider adding another index, like normalised burn index to pick up bare soils in winter.


Map.centerObject(aoi, 12)
Map.addLayer(ee.Image([autumn_nd, change_nd, winter_nd]), 
                      {min:[0, 0, 0], max: [0.8, 0.8, 0.8]}, "Aut NDVI | Aut-Win NDVI| Win NDVI");
Map.addLayer(ee.Image(change), 
                      {min:0, max: 0.6, palette:palette}, "NDVI Change");
