// ++++++++++++ CHOOSE YOUR ASSET HERE +++++++++++ //

var dbChangeMeanImage = ee.Image('users/rogierwesterhoff/dbChange2019vs2018');

var roi = dbChangeMeanImage.geometry();

Map.centerObject(roi,11);

var palettes = require('users/gena/packages:palettes');
var changeCountPalette = palettes.matplotlib.inferno[7].reverse();
var vizCount = {min: 0, max: 10, palette: changeCountPalette};
Map.addLayer(dbChangeMeanImage.divide(100).clip(roi), vizCount, 'Mean change (|dB|)', true);

// +++++ Start adding data, such as Sentinel-2 +++++ //
// We set the script up like this, because storing the S2 assets can be disk demanding
// approximately 100 GB for NZ for one nationwide mosaic.

// functions used for S2 processing
// Function to mask clouds on Sentinel-2 using the addtional 
// QA60 (option 1) or SCL and MSK_CLDPRB bands (option 2).
var maskClouds = function(image){
 
 // option 1:
 var qa = image.select('QA60');
  // Bits 10 and 11 are clouds and cirrus, respectively.
  var cloudBitMask = Math.pow(2, 10);
  var cirrusBitMask = Math.pow(2, 11);
  
  // clear if both flags set to zero.
  //var clear = qa.bitwiseAnd(cloudBitMask).eq(0);
  var clear = qa.bitwiseAnd(cloudBitMask).eq(0).and(
           qa.bitwiseAnd(cirrusBitMask).eq(0));
  
  var mask = clear.eq(1); 
 
/* 
 // I am using option 2 (only for _SR):
var cloudProb = image.select('MSK_CLDPRB');
var scl = image.select('SCL'); 

var shadow = scl.eq(3); // 3 = cloud shadow
var cirrus = scl.eq(10); // 10 = cirrus
var mask = cloudProb.lt(2).and((cirrus).neq(1)).and((shadow).neq(1)); // thanks Eric Waller for the correction
*/
return image.updateMask(mask);

};

// This function adds vegetation and water index bands to images.
var addIndexBands = function(image) {
  return image
    .addBands(image.normalizedDifference(['B8', 'B4']).rename('NDVI'));
};

// Load Sentinel-2 TOA data (Level 1).
var s2 = ee.ImageCollection('COPERNICUS/S2')
  .filterDate(startDateRef,endDateAfter)
  .filterBounds(roi);
// print('s2 ', s2);

var cloudFreeMosaicRef = s2
  .filterDate(startDateRef,endDateRef)
  .map(maskClouds)
  .select(['B4','B3','B2'])
  .median()
  .divide(10000);

var cloudFreeMosaicAfter = s2
  .filterDate(startDateAfter,endDateAfter)
  .map(maskClouds)
  .select(['B4','B3','B2'])
  .median()
  .divide(10000);

/*
var ndvi = s2
  .map(maskClouds)
  .map(addIndexBands)
  .select('NDVI')
  .median();
  
Map.addLayer(ndvi.clip(roi),
  {min: -1, max: 1, palette: ['blue', 'white', 'green']},
  'Sentinel-2 NDVI',false);
*/        

Map.addLayer(cloudFreeMosaicRef.clip(roi), {bands: ['B4', 'B3', 'B2'], min: 0, max: 0.2},
            'Sentinel-2 RGB Ref',false);
            
Map.addLayer(cloudFreeMosaicAfter.clip(roi), {bands: ['B4', 'B3', 'B2'], min: 0, max: 0.2},
            'Sentinel-2 RGB After',false);

/*
// ++++ EXPORT SENTINEL-2 DATA TO ASSET (WARNING: LARGE FILES, a few GB per region) +++++ ///
Export.image.toAsset({
  image: cloudFreeMosaicRef.clip(roi), //.toFloat(),
  description: 'cloudFreeMosaic2018',
  assetId: 'cloudFreeMosaic2018',
  scale: 10,
  region: roi,
  maxPixels: 2e12,
  pyramidingPolicy: {
    'cloudFreeMosaicRef': 'mean'
  }
});

Export.image.toAsset({
  image: cloudFreeMosaicAfter.clip(roi), //.toFloat(),
  description: 'cloudFreeMosaic2019',
  assetId: 'cloudFreeMosaic2019',
  scale: 10,
  region: roi,
  maxPixels: 2e12,
  pyramidingPolicy: {
    'cloudFreeMosaicAfter': 'mean'
  }
});


Export.image.toDrive({
  image: cloudFreeMosaicRef.clip(roi).toFloat(), //toInt
  description: 'S2CloudFreeMosaicRef',
  // crs: 'EPSG:4326',
  crs: 'EPSG:2193',
  fileFormat: 'GeoTIFF',
  scale: 10,
  region: roi,
  maxPixels: 25e9
});

Export.image.toDrive({
  image: cloudFreeMosaicAfter.clip(roi).toFloat(), //toInt
  description: 'S2CloudFreeMosaicAfter',
  // crs: 'EPSG:4326',
  crs: 'EPSG:2193',
  fileFormat: 'GeoTIFF',
  scale: 10,
  region: roi,
  maxPixels: 25e9
});
*/

//////////////////////////////////////////---Legends---///////////////////////////////////////////////
//Gradient legend from https://mygeoblog.com/2017/03/02/creating-a-gradient-legend/
// set position of panel
// create vizualization parameters
var viz = {min:0, max:10, palette: changeCountPalette};
var legend = ui.Panel({
style: {
position: 'bottom-left',
padding: '8px 15px'
}
});
 
// Create legend title
var legendTitle = ui.Label({
value: 'Change \n (|dB|)',
style: {
fontWeight: 'bold',
fontSize: '12px',
margin: '0 0 4px 0',
padding: '0',
whiteSpace: 'pre'
}
});
 
// Add the title to the panel
legend.add(legendTitle);
 
// create the legend image
var lon = ee.Image.pixelLonLat().select('latitude');
var gradient = lon.multiply((viz.max-viz.min)/100.0).add(viz.min);
var legendImage = gradient.visualize(viz);
 
// create text on top of legend
var panel = ui.Panel({
widgets: [
ui.Label(viz['max'])
],
});
 
legend.add(panel);
 
// create thumbnail from the image
var thumbnail = ui.Thumbnail({
image: legendImage,
params: {bbox:'0,0,10,100', dimensions:'10x200'},
style: {padding: '1px', position: 'bottom-center'}
});
 
// add the thumbnail to the legend
legend.add(thumbnail);
 
// create text on top of legend
var panel = ui.Panel({
widgets: [
ui.Label(viz['min'])
],
});
 
legend.add(panel);
 
Map.add(legend);